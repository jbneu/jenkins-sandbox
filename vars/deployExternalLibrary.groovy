import xyz.jbneu.sandbox.jenkins.Configuration
import xyz.jbneu.sandbox.jenkins.Helpers

def call(String configFile = "artifact.properties") {

    def config = Configuration.load(Helpers.parseProperties(readFile(configFile)))
    def errors = config.validate()
    if (!errors.isEmpty()) {
        error errors.join(", ")
        // check if neded when running in Jekins
        return
    }

    def artifact = fileExists config.artifactPath
    if(!artifact) {
        error "Artifact '${config.artifactPath}' not found"
        return
    }


    // check if git tag exist
    sshagent(credentials: ['bitbucket.org']) {
        echo "new tagName: ${config.tagName}"

        sh "git fetch --tags"
        def existingTag = sh(returnStdout: true, script: "git tag -l")
        echo "existing tags: $existingTag"

        if(config.containsTag(existingTag)){
            error "tag ${config.tagName} already exist"
        }
    }


}
