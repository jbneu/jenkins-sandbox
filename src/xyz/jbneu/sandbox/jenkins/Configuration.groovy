package xyz.jbneu.sandbox.jenkins

import java.lang.reflect.Modifier

class Configuration {
    static  def fields = Configuration.class.getDeclaredFields()
            .findAll { Modifier.toString(it.modifiers) == "public" }

    public String name
    public String group
    public String version
    public String packaging
    public String artifactPath

    static Configuration load(Properties properties) {
        def conf = new Configuration()

        fields.each {
                    it.set(conf, properties.get(it.name))
                }

        return conf
    }

    String getTagName() {
        return "v$version"
    }

    boolean containsTag(def tags) {
        return tags.contains(getTagName())
    }

    boolean isValid() {
        return fields.any {
            !it.get(this).isBlank()
        }
    }

    List<String> validate() {
        return fields.findAll{ it.get(this) == null || it.get(this).isBlank()}
                .collect { "${it.name} is required" }
    }
}


