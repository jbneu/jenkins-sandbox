package xyz.jbneu.sandbox.jenkins

import spock.lang.Shared
import spock.lang.Specification

class HelpersTest extends Specification {
    @Shared def props = null

    def setupSpec() {
        props = Helpers.parseProperties(new File(getClass().getResource('/test.properties').toURI()))
    }

    def "props are read"() {
        when:
        def name = props.name

        then:
        name == "my_name"
    }
}
