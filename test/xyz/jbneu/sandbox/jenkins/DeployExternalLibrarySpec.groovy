package xyz.jbneu.sandbox.jenkins

import testSupport.PipelineSpecification

class DeployExternalLibrarySpec extends PipelineSpecification {

    def "test step"() {

        given:
        def file = new File(getClass().getResource('/test.properties').toURI())
        helper.registerAllowedMethod("sh", [Map.class],
                { m -> return ["myTag"] })

        when:
        def script = loadScript('vars/deployExternalLibrary.groovy')
        script.call(file)

        then:
        printCallStack()
        assertJobStatusSuccess()
    }

    def "missing property"() {

        given:
        def file = new File(getClass().getResource('/missing_values.properties').toURI())

        when:
        def script = loadScript('vars/deployExternalLibrary.groovy')
        script.call(file)

        then:
        printCallStack()
        assertJobStatusFailure()
    }

}
