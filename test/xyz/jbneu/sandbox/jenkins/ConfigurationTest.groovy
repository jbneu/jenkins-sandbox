package xyz.jbneu.sandbox.jenkins

import spock.lang.Shared
import spock.lang.Specification

class ConfigurationTest extends Specification {
    @Shared def props = null
    @Shared def missing_props = null

    def setupSpec() {
        props = Helpers.parseProperties(new File(getClass().getResource('/test.properties').toURI()))
        missing_props = Helpers.parseProperties(new File(getClass().getResource('/missing_values.properties').toURI()))
    }

    def "create conf"() {
        when:
        def conf = Configuration.load(props)

        then:
        conf.name == "my_name"
    }

    def "is conf valid"() {
        given:
        def conf = Configuration.load(props)

        when:
        def res = conf.isValid()

        then:
        res
    }

    def "validate returns missing field"() {
        given:
        def conf = Configuration.load(missing_props)

        when:
        def res = conf.validate()

        then:
        res.size() == 1
    }
}
