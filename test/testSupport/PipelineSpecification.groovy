package testSupport

import spock.lang.Specification

abstract class PipelineSpecification extends Specification {

    @Delegate
    SpockPipelineTestHelper pipelineTestHelper

    def setup() {
        pipelineTestHelper = new SpockPipelineTestHelper()
        pipelineTestHelper.setUp()
    }

}
