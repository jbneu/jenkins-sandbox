FROM jenkins/jenkins:jdk11

USER root
RUN apt-get update && apt-get install -y git
# drop back to the regular jenkins user - good practice
USER jenkins
