plugins {
    groovy
    java
}

group = "xyz.jbneu.sandbox"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
    mavenCentral()
    maven("https://repo.jenkins-ci.org/releases/")
}

dependencies {
    implementation("org.codehaus.groovy:groovy-all:2.5.11")

    testImplementation(platform("org.spockframework:spock-bom:1.3-groovy-2.5"))
    testImplementation("org.spockframework:spock-core")

    testImplementation("com.lesfurets:jenkins-pipeline-unit:1.3")

    // compileOnly("org.jenkinsci.plugins", "pipeline-utility-steps", "2.5.0")
    // implementation("org.jenkins-ci.plugins:job-dsl-core:1.74")
    //compileOnly("org.jenkins-ci.plugins", "pipeline-build-step", "2.12")
    val declarativePluginsVersion = "1.3.9"
    compileOnly("org.jenkinsci.plugins", "pipeline-model-api", declarativePluginsVersion)
    /* compileOnly("org.jenkinsci.plugins", "pipeline-model-declarative-agent", "1.1.1")
    compileOnly("org.jenkinsci.plugins", "pipeline-model-definition", declarativePluginsVersion)
    compileOnly("org.jenkinsci.plugins", "pipeline-model-extensions", declarativePluginsVersion)*/
}

sourceSets {
    main {
        withConvention(GroovySourceSet::class) {
            groovy.srcDirs("vars", "src")
        }
        resources.srcDirs("resources")

    }

    test {
        withConvention(GroovySourceSet::class) {
            groovy.srcDirs("test")
        }
        resources.srcDirs("testResources")
    }
}